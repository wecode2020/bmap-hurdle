import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import axios from "axios"
import setAxios from './setAxios'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import vRegion from 'v-region';

import "./assets/scss/style.scss";

Vue.config.productionTip = true

Vue.use(ElementUI);
Vue.use(vRegion);

// Axios 
setAxios() 
Vue.prototype.$http = axios

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')

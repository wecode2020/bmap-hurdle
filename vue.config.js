const current_hurdle = require('./mock/current_hurdle.json')

module.exports = {
  publicPath: './',
  outputDir: 'dist', //build输出目录
  assetsDir: 'assets', //静态资源目录（js, css, img）
  lintOnSave: false, //是否开启eslint
  productionSourceMap: true,  // 设置上线后是否加载webpack文件
  devServer: {
    open: false, //是否自动弹出浏览器页面
    host: "127.0.0.1",
    port: '8081',
    https: false, // 是否使用https协议
    hotOnly: false, // 是否开启热更新
    before(app) {

      
      //获取当前围栏
      app.get('/api/getCurrent', (req, res) => {
        res.json(current_hurdle)
      })
    }
  }
}